package sheridan;
import static org.junit.Assert.*;
import org.junit.Test;

//HARMAN BATH 
//991540198
//SOFT MANG MIDTERM 

public class FahrenheitTest {

	
	
	@Test
	public void testFromCelsiusToFahrenheitRegular() {
		assertTrue("Invalid conversion", Farenheit.fromCelsius(10)==50);		
	}
	
	@Test
	public void testFromCelsiusToFarenheitException() {
		assertFalse("Invalid conversion", Farenheit.fromCelsius(-123)==12);
	}
	
	@Test
	public void testFromCelsiusToFahrenheitBoundaryOut() {
		assertFalse("Invalid conversion", Farenheit.fromCelsius(-1)==30);
	}
	
	@Test
	public void testFromCelsiusToFahrenheitBoundaryIn() {
		assertTrue("Invalid conversion", Farenheit.fromCelsius(2)==35); 
	}

}
